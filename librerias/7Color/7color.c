 /*	--------------------------------------------------------------------
    FILE:       7color.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes 7 color led
    Syntaxys:
		-Declare '7COLOR' type for a new led
			7COLOR t1;
		-Use global function 'atachSevenColor(7COLOR t,u8 pinS)' fro inicialize a led
			t1.atachSevenColor(&t1, pin for signal);
		-To tunr on the led use 'turnON'
			t1.turnON(&t1);	
		-To tunr off the led use 'turnOFF'
			t1.turnOFF(&t1);	

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    21-07-17 : Created
    --------------------------------------------------------------------
 
    ------------------------------------------------------------------*/
 
#include <7color.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalw.c>		//for pin mode y digitalwrite
 
	


 
/*  --------------------------------------------------------------------
    prende
    --------------------------------------------------------------------
    @descr:     enciende el led		
    @param:     *self= variable que representa el led
    @return:    none
    ------------------------------------------------------------------*/
void prende(void *self){
	7COLOR *t=self;
	digitalwrite(t->pinSig,HIGH);
    }
 
 
 
 /*  --------------------------------------------------------------------
    apaga
    --------------------------------------------------------------------
    @descr:    apaga el led
    @param:     *self= variable que representa el led
    @return:    none
    ------------------------------------------------------------------*/ 
void apaga(void *self){
	7COLOR *t=self;
	digitalwrite(t->pinSig,LOW);
    }
 
 
/*  --------------------------------------------------------------------
    atachTouch
    --------------------------------------------------------------------
    @descr:     Inicia un led con un pin designado 
    @param:     *h= variable usada por el usuario para manejar el led.
    @param:  	pinS= pin equivalente para la señal del led 
    @return:    none
    ------------------------------------------------------------------*/
void atachSevenColor(7COLOR *h,u8 pinS){
	pinmode(pinS,OUTPUT);
	h->pinSig=pinS;
	h->turnON=&prende;
	h->turnOFF=&apaga; 
 
}

