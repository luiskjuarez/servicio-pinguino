#ifndef SEVENCOLOR_H
	#define SEVENCOLOR_H
	#include <typedef.h>

 
	typedef struct seven{  
		u8 pinSig; 			
		void (*turnON)(void *self);
		void (*turnOFF)(void *self); 
		 
		
	}7COLOR;  

	void prende(void *self);	 
	void apaga(void *self);
	void atachSevenColor(7COLOR *h, u8 pinS);

#endif
