#ifndef LCUP_H
	#define LCUP_H
	#include <typedef.h>

 
	typedef struct cup{  
		u8 pinSig; 		//Pin para señal
		u8 (*readCup)(void *self);
		u16 tiempo;
		u8 status;
		int shake;
		u8 pinL;
		void (*updateCup)(void *self);
		int (*isShaking)(void *self);
		void (*writeOnCupLed)(void *self,u8 state);
	}LCUP;  

	u8 lee(void *self);
	void atachCup(LCUP *h, u8 pinS,u8 pinLed);
	void updateC(void *self);
	int shaking(void *self);
	void writeOnLed(void *self,u8 state);
#endif
