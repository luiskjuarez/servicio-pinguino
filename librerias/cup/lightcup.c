 /*	--------------------------------------------------------------------
    FILE:       lightcup.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes light cup sensor
    Syntaxys:
		-Declare 'LCUP' type for a new sensor
			LCUP t1;
		-Use global function 'atachCup(LCUP t,u8 pinS,u8 pinLed)' fro inicialize a laser
			t1.atachCup(&t1, pin for signal, pin for led);
		-To read actual signal on sensor use 'readCup'
			t1.readCup(&t1);
		-To know if the sensor is shaking use 'isShakig'
			t1.isShaking(&t1);
		-To update sensor values use 'updateCup'
			t1.updateCup(&t1);
		-To write on led of the sensor use 'writeOnCupLed'
			t1.writeOnCupLed(&t1,state);

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    29-07-17 : Created
    --------------------------------------------------------------------
 
    ------------------------------------------------------------------*/
 
#include <lightcup.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalp.c>		//for pin mode y digitalwrite
#include <digitalr.c>
#include <millis.c>		//get time
	


 
 
/*  --------------------------------------------------------------------
    shaking
    --------------------------------------------------------------------
    @descr:     regresa si esta siendo agitado o no	
    @param:     *self= variable que representa el sensor
    @return:    si esta agitado regresa 1 si no un 0
    ------------------------------------------------------------------*/
int shaking(void *self){
	LCUP *t=self;
	return (t->shake);
    } 


/*  --------------------------------------------------------------------
    updateC
    --------------------------------------------------------------------
    @descr:     actualiza los valores del sensor 
    @param:     *self= variable que representa el sensor
    @return:    none
    ------------------------------------------------------------------*/
 void updateC(void *self){
	LCUP *t=self;
	u8 auxState=t->status;
	if(millis()-(t->tiempo) >30){
		t->shake=0;			//Si dura mas de 30ms estable no esta agitado
		t->tiempo=millis();	
	}
	
	t->status=digitalread(t->pinSig);
	if((auxState==LOW && t->status==HIGH) ||(auxState==HIGH && t->status==LOW) ){
		if(millis()-(t->tiempo) < 15){   			//si ocurre un cambio en un lapso de 15ms esta siendo agitado
			t->shake=1;
			t->tiempo=millis();
		}
	}
	 
}
 
/*  --------------------------------------------------------------------
    lee
    --------------------------------------------------------------------
    @descr:     lee el sensor		
    @param:     *self= variable que representa el sensor
    @return:    alto si esta detectando movimiento, bajo si no es asi.
    ------------------------------------------------------------------*/
u8 lee(void *self){
	LCUP *t=self;
	return (digitalread(t->pinSig));
    } 


/*  --------------------------------------------------------------------
    writeOnLed
    --------------------------------------------------------------------
    @descr:     escribe un valor en el led del sensor		
    @param:     *self= variable que representa el sensor
    @param:	state= valor a escribir en el led  (HIGH,LOW);
    @return:    none.
    ------------------------------------------------------------------*/
void writeOnLed(void *self,u8 state){
	LCUP *t=self;
	digitalwrite(t->pinL,state);
}

/*  --------------------------------------------------------------------
    atachCup
    --------------------------------------------------------------------
    @descr:     Inicia un sensor ball con un pin designado 
    @param:     *h= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachCup(LCUP *h,u8 pinS,u8 pinLed){
	pinmode(pinS,INPUT);
	h->pinSig=pinS; 
	h->tiempo=0;
	h->readCup=&lee;  
	h->status=LOW;
	h->pinL=pinLed;
	h->shake=0;
	h->isShaking=&shaking;
	h->updateCup=&updateC;
	h->writeOnCupLed=&writeOnLed; 
}

