 /*	--------------------------------------------------------------------
    FILE:       flame.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes flame sensor
	
    Syntaxys:
		-Declare 'FLAME' type for a new flame sensor
			FLAME a1;
		-Use global function 'atachFlame(FLAME a,u8 pinS)' fro inicialize Tracker sensor
			atachFlame(&a1, pin for signal);
		-To get analog signal use 'analogFlame(&a1)' function	
			a1.analogFlame(&a1);
		-To get digital signal use 'digitalFlame(&a1)' function	
			a1.digitalFlame(&a1);
    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    16-07-17 : Created
    --------------------------------------------------------------------
	This library use the digital library from pinguino
    ------------------------------------------------------------------*/
 
#include <flame.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalr.c>		//For digitalread instruction
#include <analog.c>
#include <digitalp.c>	


 
/*  --------------------------------------------------------------------
    analogo
    --------------------------------------------------------------------
    @descr:     obtiene la señal analogica del sensor utilizado
    @param:     *self= variable que representa el sensor utilizado
    @return:    señal analogica comprendida entre 0 y 255
    ------------------------------------------------------------------*/
int analogo(void *self){
	FLAME *f=self;
	return  (analogread(f->pinSig));   
 }
 
 
 
 /*  --------------------------------------------------------------------
    digital
    --------------------------------------------------------------------
    @descr:     obtiene la señal digital del sensor
    @param:     *self= variable que representa el sensor utilizado
    @return:    1 o 0 (HIGH o LOW)
    ------------------------------------------------------------------*/ 
u8 digital(void *self){
	FLAME *f=self;
	return  (digitalread(f->pinSig));  
 }
 
 
/*  --------------------------------------------------------------------
    atachFlame
    --------------------------------------------------------------------
    @descr:     Inicia un sensor Flame nuevo en una variable usada por el usuario
    @param:     *f= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachFlame(FLAME *f,u8 pinS){
	pinmode(pinS,INPUT);
	f->pinSig=pinS;
	f->analogFlame=&analogo;
	f->digitalFlame=&digital;
}

