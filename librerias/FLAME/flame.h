#ifndef FLAME_H
	#define FLAME_H
	#include <typedef.h>

	//Estructura que contendra todos los atributos y funciones del sensor de linea//
	typedef struct flm{  
		u8 pinSig; 			//pin para la señal del sensor
		int (*analogFlame)(void *self);
		u8 (*digitalFlame)(void *self); 
	}FLAME;  

	int analogo(void *self);	 
	u8 digital(void *self);
	void atachFlame(FLAME *t, u8 pinS);

#endif
