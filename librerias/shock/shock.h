#ifndef SHOCK_H
	#define SHOCK_H
	#include <typedef.h>

 
	typedef struct shk{  
		u8 pinSig; 		//Pin para señal
		u8 (*readShock)(void *self);
		u16 tiempo;
		u8 status;
		int shake;
		void (*updateShock)(void *self);
		int (*isShaking)(void *self);
	}SHOCK;  

	u8 lee(void *self);
	void atachSchock(SHOCK *h, u8 pinS);
	void updateS(void *self);
	int shaking(void *self);
#endif
