#ifndef STEPPER_H
	#define STEPPER_H
	#include <typedef.h> 
 
	#define IZQ	1
	#define DER	0
	#define MEDIUM	0
	#define FULL	1

	const int paso1[4]={1,0,0,0};
	const int paso2[4]={1,1,0,0};
	const int paso3[4]={0,1,0,0};
	const int paso4[4]={0,1,1,0};
	const int paso5[4]={0,0,1,0};
	const int paso6[4]={0,0,1,1};
	const int paso7[4]={0,0,0,1};
	const int paso8[4]={1,0,0,1};


	typedef struct stpr{  
		u8 pin1;
		u8 pin2;
		u8 pin3;
		u8 pin4;
		int medio;
		int velocidad;
		void (*runStepper)(void *self,int steps,int dir);
		void (*setStepVelocity)(void *self,int vel); 
		void (*setStepRate)(void *self,int stepType);
	}STEPPER;   

	void atachStepper(STEPPER *t, u8 pinm1,u8 pinm2,u8 pinm3,u8 pinm4);
	void ponVelocidad(void *self,int vel);
	void correStepper(void *self,int pasos,int dir);
	void ponTipoPaso(void *self,int stepType);
#endif
