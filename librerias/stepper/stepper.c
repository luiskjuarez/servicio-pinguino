 /*	--------------------------------------------------------------------
    FILE:       stepper.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a stepper motor
	
    Syntaxys:
		-Declare 'STEPPER' type for a new motor
			STEPPER t1;
		-Use global function 'atachStepper(STEPPER t,u8 pin1,u8 pin2,u8 pin3, u8 pin4)' for inicialize Servomotor 
			t1.atachStepper(&t1, pin 1, pin 2, pin 3, pin 4);
		-To set velocity in the motor use 'setStepVelocity'
			t1.setStepVelocity(&t1,velocity);
		-To run the motor use 'runStepper(void *self,int steps,int dir)'
			t1.runStepper(&t,2434,DER);


    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    24-08-17 : Created
    --------------------------------------------------------------------
 	This library will change to be able to control more than one servo at the same time, because
	actually first move one by one.
    ------------------------------------------------------------------*/
 
#include <stepper.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <analog.c>		//For digitalread instruction
#include <digitalp.c>
#include <delayus.c>	
#include <digitalw.c>
#include <digital.h>


/*  --------------------------------------------------------------------
    atachDCMotor
    --------------------------------------------------------------------
    @descr:     Inicia un motor 
    @param:     *t= variable usada por el usuario para manejar el motor.
    @param:  	pinm1= Pin para hilo 1  
    @param:  	pinm1= Pin para hilo 2  
    @param:  	pinm1= Pin para hilo 3  
    @param:  	pinm1= Pin para hilo 4  
    @return:    none
    ------------------------------------------------------------------*/
void atachStepper(STEPPER *t, u8 pinm1,u8 pinm2,u8 pinm3,u8 pinm4){
	t->pin1=pinm1;
	t->pin2=pinm2;
	t->pin3=pinm3;
	t->pin4=pinm4;
	t->velocidad=20;
	pinmode(pinm1,OUTPUT);
	pinmode(pinm2,OUTPUT); 
	t->setStepVelocity=&ponVelocidad;
	t->runStepper=&correStepper; 	
	t->setStepRate=&ponTipoPaso;
}



/*  --------------------------------------------------------------------
    ponVelocidad
    --------------------------------------------------------------------
    @descr:     Asigna una velocidad
    @param:     *t= variable usada por el usuario para manejar el servo.
    @param:  	vel= velocidad para motor
    @return:    none
    ------------------------------------------------------------------*/
void ponVelocidad(void *self,int vel){
	STEPPER *t=self;
	t->velocidad=vel;
}



/*  --------------------------------------------------------------------
    ponTipoPaso
    --------------------------------------------------------------------
    @descr:     Coloca el tipo de paso que dara el motor (MEDIUM, FULL)
    @param:     *self= variable usada por el usuario para manejar el motor.
    @param:  	stepType = tipo del paso.
    @return:    none
    ------------------------------------------------------------------*/
 void ponTipoPaso(void *self,int stepType){
	STEPPER *t=self;
	t->medio=stepType;
}



/*  --------------------------------------------------------------------
    correStepper
    --------------------------------------------------------------------
    @descr:     hace mover el motor un numero de pasos en una direccion
    @param:     *t= variable usada por el usuario para manejar el motor.
    @param:  	pasos= numero de pasos que dara el motor 
    @param:  	dir= direccion dle motor
    @return:    none
    ------------------------------------------------------------------*/
 void correStepper(void *self,int pasos,int dir){
	int i;
	STEPPER *t=self;
	int i2;
	int delayM;
	int sum;
	i2=0;
	sum=2;
	
	delayM = pasos/t->velocidad;

	for(i=pasos;i>=0;i--){
		switch(i2){
			case 1:
				digitalwrite(t->pin1,paso1[0]);
				digitalwrite(t->pin2,paso1[1]);
				digitalwrite(t->pin3,paso1[2]);
				digitalwrite(t->pin4,paso1[3]);
				break;	
			case 2:
				digitalwrite(t->pin1,paso2[0]);
				digitalwrite(t->pin2,paso2[1]);
				digitalwrite(t->pin3,paso2[2]);
				digitalwrite(t->pin4,paso2[3]);
				break;	
			case 8:
				digitalwrite(t->pin1,paso8[0]);
				digitalwrite(t->pin2,paso8[1]);
				digitalwrite(t->pin3,paso8[2]);
				digitalwrite(t->pin4,paso8[3]);
				break;	
			case 3:	
				digitalwrite(t->pin1,paso3[0]);
				digitalwrite(t->pin2,paso3[1]);
				digitalwrite(t->pin3,paso3[2]);
				digitalwrite(t->pin4,paso3[3]);
				break;	
			case 4:
				digitalwrite(t->pin1,paso4[0]);
				digitalwrite(t->pin2,paso4[1]);
				digitalwrite(t->pin3,paso4[2]);
				digitalwrite(t->pin4,paso4[3]);
				break;	
			case 5:
				digitalwrite(t->pin1,paso5[0]);
				digitalwrite(t->pin2,paso5[1]);
				digitalwrite(t->pin3,paso5[2]);
				digitalwrite(t->pin4,paso5[3]);
				break;	
			case 6:
				digitalwrite(t->pin1,paso6[0]);
				digitalwrite(t->pin2,paso6[1]);
				digitalwrite(t->pin3,paso6[2]);
				digitalwrite(t->pin4,paso6[3]);
				break;	
			case 7:
				digitalwrite(t->pin1,paso7[0]);
				digitalwrite(t->pin2,paso7[1]);
				digitalwrite(t->pin3,paso7[2]);
				digitalwrite(t->pin4,paso7[3]);
				break;		
			
			}
		Delayus(delayM);
		if(t->medio==1)
				sum=1;
		if(dir==IZQ){
			i2-=sum;
			if(i2<0){
				if(t->medio==1)
					i2=8;
				else
					i2=7;
				}
		}
		else{
			i2+=sum;
			if(i2>8)
				i2=1;
		}
		
	} 
}




