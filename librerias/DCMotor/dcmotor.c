 /*	--------------------------------------------------------------------
    FILE:       dcmotor.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a dc motor
	
    Syntaxys:
		-Declare 'DCMOTOR' type for a new motor
			DCMOTOR t1;
		-Use global function 'atachDCMotor(DCMOTORVO t,u8 pin1,u8 pin2)' for inicialize Servomotor 
			t1.atachDCMotor(&t1, pin 1, pin 2);
		-To turn left use 'turnLeft'
			t1.turnLeft(&t1);
		-To turn right use 'turnRight'
			t1.turnRight(&t1);	
		-To stop 'stop'
			t1.stop(&t1);
		-To change velocity use 'setVelocity(DCMotor t1, int velocity)'
			t1.setVelocity(&t1, velocity);
			!!This function is only for PWM pins, so you can only use 1 motor
			  (because board hae 2 pwm signals).¡¡
			Velocity 0-2


    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    16-08-17 : Created
    --------------------------------------------------------------------
 	
    ------------------------------------------------------------------*/
 
#include <dcmotor.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <analog.c>		//For digitalread instruction
#include <digitalp.c>
#include <pwm.c>	
#include <digitalw.c>
 


/*  --------------------------------------------------------------------
    atachDCMotor
    --------------------------------------------------------------------
    @descr:     Inicia un motor ocon dos pines
    @param:     *t= variable usada por el usuario para manejar el motor.
    @param:  	pinm1= pin equivalente para la señal del hilo 1 del motor 
    @param:  	pinm2= pin equivalente para la señal del hilo 2 del motor 
    @return:    none
    ------------------------------------------------------------------*/
void atachDCMotor(DCMOTOR *t,u8 pinm1,u8 pinm2){
	t->pin1=pinm1;
	t->pin2=pinm2;
	t->velocidad=2; //velocidad maxima 	 
	t->banGiro=1;	//a la izquierda
	pinmode(pinm1,OUTPUT);
	pinmode(pinm2,OUTPUT); 
	para(t);
	t->run=&corre;
	t->stop=&para
	t->turnLeft=&izq;
	t->turnRight=&der;
	t->setVelocity=&ponVelocidad;
} 



/*  --------------------------------------------------------------------
    para
    --------------------------------------------------------------------
    @descr:     detiene un motor
    @param:     *t= variable usada por el usuario para manejar el motor.
    @return:    none
    ------------------------------------------------------------------*/
void para(void *self){
	DCMOTOR *t=self; 
	if(t->pin1!=11 && t->pin1!= 12)
		digitalwrite(t->pin1,HIGH);
	else
		analogwrite(t->pin1,1023);	
	
	if(t->pin2!=11 && t->pin2!= 12)
		digitalwrite(t->pin2,HIGH);
	else
		analogwrite(t->pin2,1023);	
}



/*  --------------------------------------------------------------------
    corre
    --------------------------------------------------------------------
    @descr:     Vuelve a poner en marcha un motor
    @param:     *t= variable usada por el usuario para manejar el motor.
    @return:    none
    ------------------------------------------------------------------*/
void corre(void *self){
	DCMOTOR *t=self; 
	if(t->banGiro==1)
		izq(t);
	else
		der(t);
}




/*  --------------------------------------------------------------------
    izq
    --------------------------------------------------------------------
    @descr:     hace girar en un sentido el motor
    @param:     *t= variable usada por el usuario para manejar el motor.
    @return:    none
    ------------------------------------------------------------------*/	
void izq(void *self){
	DCMOTOR *t=self; 
	int sum;
	sum=(2-(t->velocidad));
	sum=sum*150;
	if(t->pin1!=11 && t->pin1!= 12)
		digitalwrite(t->pin1,HIGH);
	else
		analogwrite(t->pin1,(1023-sum));	
	
	if(t->pin2!=11 && t->pin2!= 12)
		digitalwrite(t->pin2,LOW);
	else
		analogwrite(t->pin2,(0+sum));		
	t->banGiro=1;
}





/*  --------------------------------------------------------------------
    der
    --------------------------------------------------------------------
    @descr:     hace girar en otro senido al motor
    @param:     *t= variable usada por el usuario para manejar el motor.
    @return:    none
    ------------------------------------------------------------------*/
void der(void *self){
	DCMOTOR *t=self; 
	int sum;
	sum=(2-(t->velocidad));
	sum=sum*150;


	if(t->pin1!=11 && t->pin1!= 12)
		digitalwrite(t->pin1,LOW);
	else
		analogwrite(t->pin1,(0+sum));	
	
	if(t->pin2!=11 && t->pin2!= 12)
		digitalwrite(t->pin2,HIGH);
	else
		analogwrite(t->pin2,(1023-sum));

	t->banGiro=0;	
}
 



/*  --------------------------------------------------------------------
    ponVelocidad
    --------------------------------------------------------------------
    @descr:     coloca una velocidad en el motor (si puede cambiarla, si no no hace nada)
    @param:     *t= variable usada por el usuario para manejar el motor.
    @return:    none
    ------------------------------------------------------------------*/
void ponVelocidad(void *self,int vel){
	DCMOTOR *t=self; 
	
	
	if((t->pin1==11 && t->pin2==12) || (t->pin1==12 && t->pin2==11)){
		if(vel>2)
			t->velocidad=2;
		else
			if(vel<0)
				t->velocidad=0;
			else
				t->velocidad=vel;
	}
}

