#ifndef DCM_H
	#define DCM_H
	#include <typedef.h> 
 
	typedef struct motordc{  
		u8 pin1;
		u8 pin2;
		int banGiro;
		int velocidad;
		void (*run)(void *self);
		void (*stop)(void *self);		
		void (*turnLeft)(void *self);
		void (*turnRight)(void *self);
		void (*setVelocity)(void *self,int vel); 
	
	}DCMOTOR;  
	
	
	void para(void *self);
	void izq(void *self);
	void der(void *self);
	void atachDCMotor(DCMOTOR *t, u8 pinm1,u8 pinm2);
	void ponVelocidad(void *self,int vel);
	void corre(void *self);
#endif
