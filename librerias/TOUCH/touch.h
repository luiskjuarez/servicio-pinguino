#ifndef TOUCH_H
	#define TOUCH_H
	#include <typedef.h>

	//Estructura que contendra todos los atributos y funciones del sensor
	typedef struct touch{  
		u8 pinSig; 			//pin para la señal del sensor
		int (*isTouch)(void *self);
 
	}TOUCH;  

	int analogo(void *self); 
	void atachTouch(TOUCH *t, u8 pinS);

#endif
