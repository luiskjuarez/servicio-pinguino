 /*	--------------------------------------------------------------------
    FILE:       touch.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for touch sensor
	
    Syntaxys:
		-Declare 'TOUCH' type for a new toush sensor
			TOUCH t1;
		-Use global function 'atachTouch(TOUCH t,u8 pinS)' fro inicialize touch sensor
			t1.atachTouch(&t1, pin for signal);
		-To know if the sensor is touched use 'isTouch' 
			t1.isTouch(&t1);	

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    19-07-17 : Created
    --------------------------------------------------------------------
 
    ------------------------------------------------------------------*/
 
#include <touch.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalr.c>		//For digitalread instruction
#include <analog.c>
#include <digitalp.c>	


 
/*  --------------------------------------------------------------------
    analogo
    --------------------------------------------------------------------
    @descr:     obtiene la señal analogica del sensor utilizado
    @param:     *self= variable que representa el sensor utilizado
    @return:    0 si no esta siendo tocado 1 si esta tocado
    ------------------------------------------------------------------*/
int analogo(void *self){
	TOUCH *t=self;
	int val;
	if(t->pinS>=13 && t->pinS<=19)
		val=analogread(t->pinS);
	else
		val=digitalread(t->pinS);

	if(val==HIGH || (val>=900 && val <=1023)
		return 0;
	else
		return 1;
 }
 
 
 
/*  --------------------------------------------------------------------
    atachTouch
    --------------------------------------------------------------------
    @descr:     Inicia un sensor touch nuevo en una variable usada por el usuario
    @param:     *t= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachTouch(TOUCH *t,u8 pinS){
	t->pinSig=pinS;
	pinmode(pinS,INPUT);
	t->isTouch=&analogo;
 
}

