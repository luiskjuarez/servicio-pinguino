#ifndef LASER_H
	#define LASER_H
	#include <typedef.h>

 
	typedef struct lsr{  
		u8 pinSig; 			
		void (*turnON)(void *self);
		void (*turnOFF)(void *self); 
		void(*turnONTime)(void *self,unsigned long tie);
		void(*updateLaser)(void *self);
		unsigned long tiempoEncendido;
		unsigned long tiempoTrans;
		int banEnc; 
		
	}LASER;  

	void prende(void *self);	 
	void apaga(void *self);
	void atachLaser(LASER *h, u8 pinS);
	void prendeTiem(void *self,unsigned long tie);
	void updateL(void *self);
#endif
