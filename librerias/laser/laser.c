 /*	--------------------------------------------------------------------
    FILE:       laser.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes laser emit
    Syntaxys:
		-Declare 'LASER' type for a new led
			LASER t1;
		-Use global function 'atachLaser(LASER t,u8 pinS)' fro inicialize a laser
			t1.atachLaser(&t1, pin for signal);
		-To tunr on the led use 'turnON'
			t1.turnON(&t1);	
		-To tunr off the led use 'turnOFF'
			t1.turnOFF(&t1);
		-To turn the laser for a time use 'turnONTime'
			t1.turnONTime(&t1,time in ms);	
		-To update the laser values use 'updateLaser'
			t1.updateLaser(&t1);
			¡¡this function should be at the loop !!
		
    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    03-08-17 : Created
    --------------------------------------------------------------------
 
    ------------------------------------------------------------------*/
#define __MILLIS__
	#include <millis.c> 
#include <laser.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalw.c>		//for pin mode y digitalwrite


 
/*  --------------------------------------------------------------------
    updateL
    --------------------------------------------------------------------
    @descr:     esta verificando el tiempo transcurrido		
    @param:     *self= variable que representa el laser
    @return:    none
    ------------------------------------------------------------------*/
void updateL(void *self){
	LASER *t=self;
	if(t->banEnc==1){
		if(millis()-(t->tiempoTrans) > t->tiempoEncendido){
			digitalwrite(t->pinSig,LOW);
			t->banEnc=0;
			t->tiempoTrans=0;
			t->tiempoEncendido=0;
		}
	}
 
    }
 
	


 
/*  --------------------------------------------------------------------
    prendeTiem
    --------------------------------------------------------------------
    @descr:     enciende el laser por un tiempo definido (las demas funciones se desactivan en ese lapso)		
    @param:     *self= variable que representa el laser
    @param: 	tie= tiempo de encendido en ms
    @return:    none
    ------------------------------------------------------------------*/
void prendeTiem(void *self,unsigned long tie){
	LASER *t=self;
	t->tiempoTrans=millis();
	t->tiempoEncendido=tie;
	t->banEnc=1;
	digitalwrite(t->pinSig,HIGH);	 
    }
 
	
 
/*  --------------------------------------------------------------------
    prende
    --------------------------------------------------------------------
    @descr:     enciende el laser		
    @param:     *self= variable que representa el laser
    @return:    none
    ------------------------------------------------------------------*/
void prende(void *self){
	LASER *t=self;
	if(t->banEnc==0)
		digitalwrite(t->pinSig,HIGH);
    }
 
 
 
 /*  --------------------------------------------------------------------
    apaga
    --------------------------------------------------------------------
    @descr:    apaga el laser
    @param:     *self= variable que representa el laser
    @return:    none
    ------------------------------------------------------------------*/ 
void apaga(void *self){
	LASER *t=self;
	if(t->banEnc==0)	
		digitalwrite(t->pinSig,LOW);
    }
 
 
/*  --------------------------------------------------------------------
    atachLaser
    --------------------------------------------------------------------
    @descr:     Inicia un laser con un pin designado 
    @param:     *h= variable usada por el usuario para manejar el laser.
    @param:  	pinS= pin equivalente para la señal del led 
    @return:    none
    ------------------------------------------------------------------*/
void atachLaser(LASER *h,u8 pinS){
	pinmode(pinS,OUTPUT);
	h->pinSig=pinS;
	h->turnON=&prende;
	h->turnOFF=&apaga; 
	h->tiempoEncendido=0;
	h->tiempoTrans=0;
	h->banEnc=0;
	h->turnONTime=&prendeTiem;
 	h->updateLaser=&updateL;
}

