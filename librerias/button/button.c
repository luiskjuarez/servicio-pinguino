 /*	--------------------------------------------------------------------
    FILE:       button.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a button
    Syntaxys:
		-Declare 'BUTTON' type for a new button
			BUTTON t1;
		-Use global function 'atacBhutton(BUTTON t,u8 pinS)' fro inicialize a laser
			t1.atachButton(&t1, pin for signal);
		-To read actual signal on sensor use 'isPressed'
			t1.isPressed(&t1);
		-To read the time of the last press use 'lastPressedTime'
			t1.lastPressedTime(&t1);
		-To read the number of times it was pressed use 'timesPressed'
			t1.timesPressed(&t1);
		-To update status of button use 'update'
			t1.update(&t1);
			¡¡This function should be at loop from pinguino!!

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    08-08-17 : Created
    --------------------------------------------------------------------
 
    ------------------------------------------------------------------*/
#include <millis.c>		//get time
#include <button.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ...  
#include <digitalp.c>	//for pin mode 
#include <digitalr.c>

 
/*  --------------------------------------------------------------------
    presionado
    --------------------------------------------------------------------
    @descr:     indica si el boton esta preionado o no		
    @param:     *self= variable que representa el boton
    @return:    alto si esta presionado bajo si no.
    ------------------------------------------------------------------*/
 
u8 presionado(void *self){
	BUTTON *t=self;
	return (digitalread(t->pinSig));
}


/*  --------------------------------------------------------------------
    tiempo
    --------------------------------------------------------------------
    @descr:     Devuelve el tiempo que duro la ultima pulsasion.
    @param:     *self= variable usada por el usuario para manejar el boton. 
    @return:    tiempo en segundos.
    ------------------------------------------------------------------*/
float tiempo (void *self){
	BUTTON *t=self;
	return (t->lastTime/1000);	//el tiempo esta almacenado en milisegundos se pasa a segundos

}

/*  --------------------------------------------------------------------
    veces
    --------------------------------------------------------------------
    @descr:     devueve el numero de veces que se presiono hasta ahora el boton
    @param:     *self= variable usada por el usuario para manejar el boton.
    @return:    numero de veces presionado
    ------------------------------------------------------------------*/
u16 vecesF(void *self){
	BUTTON *t=self;
	return (t->veces);
}


/*  --------------------------------------------------------------------
    actualizaBoton
    --------------------------------------------------------------------
    @descr:     actualiza el estado de todas las variables del boton
    @param:     *self= variable usada por el usuario para manejar el boton.
    @return:    none
    ------------------------------------------------------------------*/
void actualizaBoton(void *self){
	BUTTON *t=self;
	t->status=digitalread(t->pinSig); //actualiza el estado actual del sensor
	
	if(t->status==HIGH && t->bn==0){  //si el estado del boton es presionado por primera vez (ban=0)   
		t->tiempo1=millis();	  //se almacena un tiempo de inicio para saber cuando se presiono
		t->ban=1;		  //se activa la bander apara sabe que ya fue presionado
	}
	else
		if(t->ban==1 && t->status == LOW) //si se solto el boton y antes estaba presionado
			{	
			t->lastTime=millis()-(t->tiempo1);  //se calcula el tiempo que duro presionado 
			t->veces++;			    //se incrementa el numero de veces preisonado
			t->ban=0;		            //se desactiva la pbandera (indica que no esta presionado)
		}	

}
/*  --------------------------------------------------------------------
    atachButton
    --------------------------------------------------------------------
    @descr:     Inicia un boton 
    @param:     *h= variable usada por el usuario para manejar el boton.
    @param:  	pinS= pin equivalente para la señal del boton 
    @return:    none
    ------------------------------------------------------------------*/
void atachButton(BUTTON *h,u8 pinS){
	pinmode(pinS,INPUT);
	h->pinSig=pinS; 
	h->veces=0;
	h->lastTime=0;
	h->isPressed=&presionado;  
 	h->lastPressedTime=&tiempo;
	h->timesPressed=&vecesF;
	h->update=&actualizaBoton;
	h->tiempo1=0; 
	h->ban=0;
}

