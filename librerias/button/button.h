#ifndef BUTTON_H
	#define BUTTON_H
	#include <typedef.h>

 	//etructura para el boton
	typedef struct btn{  
		u8 pinSig; 				//Pin para señal
		u16 tiempo1;				//tiempo donde inicia a presionar
		u16 veces;				//veces presionado
		u8 status;				//estado actual del boton
		u16 lastTime;				//ultimo tiempo presionado
		int ban;				//indica si se presiona o no
		u8 (*isPressed)(void *self);
		float (*lastPressedTime)(void *self);
		u16 (*timesPressed)(void *self);
		void (*update)(void *self);
	}BUTTON;

	u8 presionado(void *self);
	void atachButton(BUTTON *h, u8 pinS);
	float tiempo (void *self);
	u16 vecesF(void *self);
	void actualizaBoton(void *self);
#endif
