 /*	--------------------------------------------------------------------
    FILE:       rotaryE.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a rotary encoder, this can return the actual position
		and the last direction.
    Syntaxys:
		-Declare 'ENCODER' type for a new encoder
			ENCODER e1;
		-Use global function 'atachEncoder(ENCODER e,u8 dt,u8 clk,u8 sw)' fro inicialize joystick
			atachJoy(&e1, pin for dt, pin for clk, pin for Switch (-1 if not used))
		-Use the function 'refresh' within the loop this function for refresh the encoder
		 values.
			e1.refresh(&e1);
			      
		-To get last directin use the function 'getLastDirection'
			e1.getLastDirection(&e1);
		-To get position use the function 'getPosition'
			e1.getPosition(&e1);
		-To know the button status use the function 'isPressedButton'
			e1.isPressedButton(&e1);


    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    09-07-17 : Created
    --------------------------------------------------------------------
	This library use the analog, digital libraries from pinguino
    ------------------------------------------------------------------*/
 
#include <rotaryE.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <analog.c>		//For analogread instruction
#include <digitalr.c> 



/*  --------------------------------------------------------------------
    inicializeAllPin
    --------------------------------------------------------------------
    @descr:     inicia las funciones y campos de la estuctura del encoder
    @param:     *e= variable usada por el usuario para manejar el joystick
    @param:  	dt= pin equivalente para dt
    @param:  	clk= pin equivalente para clk
    @param:	sw= pin equivalente para el switch del encoder
    @return:    none
    ------------------------------------------------------------------*/
void inicializeAllPin(void *e,u8 dt,u8 clk, u8 sw){
	ENCODER *en=e;
	en->DT=dt;
	en->CLK=clk;
	en->encoderButton=sw;
	en->pos=0;
	en->clkActual=0;
	en->lastDir=0;
	 
}
 

/*  --------------------------------------------------------------------
    actualizaDir
    --------------------------------------------------------------------
    @descr:     verifica el estado de los pines dt y clk y segun un estado anterior se actualiza
		esto es para determinar la direccion y la posicion
    @param:     *e= variable usada por el usuario para manejar el joystick
    @return:    none
    ------------------------------------------------------------------*/
void actualizaDir(void *e){
	ENCODER *en=e;
	int act2=digitalread(en->CLK);
	if(act2!= en->clkActual){
        
		if(digitalread(en->DT)!=act2){
		    	en->pos++;
			if(en->pos==41)
				en->pos=0;
			en->lastDir=1;
		}
		else{ 
		    	en->pos--;
		 	if(en->pos==-1)
				en->pos=40;
		    	en->lastDir=0;	
		}   
        } 
	en->clkActual=act2;
}

/*  --------------------------------------------------------------------
    isPressed
    --------------------------------------------------------------------
    @descr:     verifica si el switch del encoder se esta presionando
    @param:     *e= variable usada por el usuario para manejar el joystick
    @return:    0 si no se presiono, 1 si se presiono
    ------------------------------------------------------------------*/
int isPressed(void *en ){
	ENCODER *e=en;
	if(  digitalread(e->encoderButton)==HIGH)
		return 1;
	else
		return 0;
}

/*  --------------------------------------------------------------------
    getDir
    --------------------------------------------------------------------
    @descr:     regresa la ultima dirccion en la que se movio el encoder
    @param:     *e= variable usada por el usuario para manejar el joystick
    @return:    ultima direccion registrada
    ------------------------------------------------------------------*/
int getDir(void *en ){
	ENCODER *e=en;
	return (e->lastDir);
}

/*  --------------------------------------------------------------------
    getPos
    --------------------------------------------------------------------
    @descr:     regresa la posicion en la que se encuentra el encoder
		se deber amultiplicar por 9 paa transformar a angulo, ya que el encoder 
		tiene 40 posiciones 369/40 = 9
    @param:     *e= variable usada por el usuario para manejar el joystick
    @return:    Posicion guardada en la estructura multimplicada por 9
    ------------------------------------------------------------------*/
int getPos( void *en){
	ENCODER *e=en;
	return ((e->pos)*9);
}


/*  --------------------------------------------------------------------
    atachEncoder
    --------------------------------------------------------------------
    @descr:     Inicia un encoder nuevo en una variable usada por el usuario
    @param:     *e= variable usada por el usuario para manejar el joystick
    @param:  	dt= pin equivalente para dt
    @param:  	clk= pin equivalente para clk
    @param:	sw= pin equivalente para el switch del encoder
    @return:    none
    ------------------------------------------------------------------*/
void atachEncoder(ENCODER *e,u8 dt, u8 clk, u8 sw){

	e->getLastDirection=&getDir;
	e->getPosition=&getPos;
	e->isPressedButton=&isPressed;
	inicializeAllPin(e,dt,clk,sw);
	e->refresh=&actualizaDir;
}

