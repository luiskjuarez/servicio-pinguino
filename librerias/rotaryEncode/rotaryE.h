#ifndef ROTARYE_H
	#define ROTARYE_H
	#include <typedef.h>
	#include <analog.c>

	
	//Estructura que contendra todos los atributos y funciones del Encoder//
	typedef struct enc{ 
		u8 encoderButton; 	//Pin del boton
		u8 DT;		//Pin de DT
		u8 CLK;		//Pin de CLK
		int pos;	//Posicion o conteo del encoder
		int clkActual;	//lectura de clk actual
		int lastDir;	//ultima direccion registrada		
		void (*inicializeAllPins) (void *self,u8 dt, u8 clk, u8 sw); 
		int (*isPressedButton)(void *self);
		int (*getLastDirection)(void *self);
		int (*getPosition)(void *self);
		void (*refresh)(void *self);
	}ENCODER;  

	void inicializeAllPin(void *e,u8 dt,u8 clk, u8 sw); 	 
	int isPressed(void *en );
	int getDir(void *en ); 
	int getPos( void *en); 
	void atachEncoder(ENCODER *e,u8 dt, u8 clk, u8 sw);
	void actualizaDir(void *e);
#endif
