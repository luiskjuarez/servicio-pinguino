#ifndef HEARTBEAT_H
	#define HEARTBEAT_H
	#include <typedef.h>

	//Estructura que contendra todos los atributos y funciones del sensor //
	typedef struct hbeat{  
		u8 pinSig; 			//pin para la señal del sensor
		int (*readBPM)(void *self);
		void (*update)(void *self); 
		double valAnt;			//Valor de bpm actual
		
	}HBEAT;  

	int analogo(void *self);	 
	void actualiza(void *self);
	void atachHeartBeat(HBEAT *h, u8 pinS);

#endif
