 /*	--------------------------------------------------------------------
    FILE:       heartbeat.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes heartbeat sensor
    Syntaxys:
		-Declare 'HBEAT' type for a new heartbeat sensor
			HBEAT t1;
		-Use global function 'atachHeartBeat(HBEAT t,u8 pinS)' fro inicialize heart beat sensor
			t1.atachHeartBeat(&t1, pin for signal);
		-To read Beats per minute use 'readBPM'
			t1.readBPM(&t1);	
		-To update the read of sensor use 'update'
			t1.update(&t1);
			!!This function should be in the loop function¡¡ 

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    20-07-17 : Created
    --------------------------------------------------------------------
	This library use the digital library from pinguino
    ------------------------------------------------------------------*/
 
#include <heartbeat.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalw.c>		//for pin mode 
#include <analog.c>		//for analogread
	


 
/*  --------------------------------------------------------------------
    analogo
    --------------------------------------------------------------------
    @descr:     obtiene el valor actual de bpm (es por 30 segundos aprox)
		lo devuelve al doble.
    @param:     *self= variable que representa el sensor utilizado
    @return:    valor de bpm actual.
    ------------------------------------------------------------------*/
int analogo(void *self){
	HBEAT *t=self;
	return (t->valAnt * 2);
	

 }
 
 
 
 /*  --------------------------------------------------------------------
    actualiza
    --------------------------------------------------------------------
    @descr:     actualiza el valor de bpm segun la lectura del sensor
    @param:     *self= variable que representa el sensor utilizado
    @return:    none
    ------------------------------------------------------------------*/ 
void actualiza(void *self){
	HBEAT *t=self;
	int val=analogread(t->pinSig); 
	t->valAnt= .75*t->valAnt+ .25*val; 
 }
 
 
/*  --------------------------------------------------------------------
    atachHeartBeat
    --------------------------------------------------------------------
    @descr:     Inicia un sensor heart beat nuevo en una variable usada por el usuario
    @param:     *h= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachHeartBeat(HBEAT *h,u8 pinS){
	pinmode(pinS,INPUT);
	h->pinSig=pinS;
	h->readBPM=&analogo;
	h->update=&actualiza; 
	h->valAnt=0;
}

