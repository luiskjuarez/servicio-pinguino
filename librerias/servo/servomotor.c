 /*	--------------------------------------------------------------------
    FILE:       servomotor.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a servomotor
	
    Syntaxys:
		-Declare 'SERVO' type for a new servomotor
			SERVO t1;
		-Use global function 'atachServo(SERVO t,u8 pinS)' for inicialize Servomotor 
			t1.atachServo(&t1, pin for signal);
		-To write gegrees on the servo use the function 'write' 
			t1.write(&t1,degrees);	
		-To set new values for times an degrees use 'setRange'	
			t1.setRange(&t1,minimum Microseconds,maximum microseocnds,minimum degrees,maximum degrees)

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    17-08-17 : Created
    --------------------------------------------------------------------
 	This library will change to be able to control more than one servo at the same time, because
	actually first move one by one.
    ------------------------------------------------------------------*/
 
#include <servomotor.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalw.c>		//For digitalread instruction
#include <digitalp.c>
#include <delayus.c>	
#include <delayms.c>
#include <mathlib.c>
 
/*  --------------------------------------------------------------------
    escribe
    --------------------------------------------------------------------
    @descr:     posiciona el servomotor en un angulo dado
    @param:     *self= variable que representa el servo
    @param: 	grados= grados a los que se tiene que posicionar el servomotor
    @return:    none
    ------------------------------------------------------------------*/
void escribe(void *self,int grados){
	SERVO *t=self;
	long milseg;
	int i=50; //50hz
	milseg=mapea(grados,t->minDeg,t->maxDeg,t->minMicro,t->maxMicro); 
	if(milseg>t->maxMicro)
		milseg=t->maxMicro;
	else
		if(milseg<t->minMicro)
			milseg=t->minMicro;
	while(i>=0){
		i--;		
	 	digitalwrite(t->pinSig,HIGH);
	   	Delayus((int)milseg);
	   	digitalwrite(t->pinSig,LOW); 
		Delayus(25000-(int)milseg);
		//Delayms(20);
	}
        Delayms(25);

 
}
 
 
/*  --------------------------------------------------------------------
    map
    --------------------------------------------------------------------
    @descr:     mapea un valor de grados a microsegundos
    @param:	x= valor a mapear (angulo).
    @param:	in_min= el minimo de grados para el servo
    @param:	in_max= el maximo de grados para el servo
    @param:	out_min= el minimo de pulsaciones para el servo (microsegundos)
    @param:	out_max= el maximo de pulsaciones para el servo (microsegundos)
    @return:    tiempo que debera durar el ancho de pulso (mapeado en microsegundos)
    ------------------------------------------------------------------*/
long mapea(int x,int in_min,int in_max,int out_min,int out_max){ 
	long n1=(x - in_min);
	long n2 =(out_max - out_min);
	long n3=(in_max - in_min) ;
	return (n1*n2/n3)+out_min;
 }
 

/*  --------------------------------------------------------------------
    ponRango
    --------------------------------------------------------------------
    @descr:     coloca un nuevo rango de valores para grados y pulsaciones
    @param:     *self= variable que representa el servo 
    @param:	minM= el minimo de tiempo para las pulsaciones del servo (en microsegundos)
    @param:	maxM= el maximo de tiempo para las pulsaciones del servo (en microsegundos)
    @param:	minD= el minimo de grados para el servo 
    @param:	maxD= el maximo de grados para el servo 
    @return:    none
    ------------------------------------------------------------------*/
void ponRango(void *self,int minD,int maxD,int minM,int maxM){
	SERVO *t=self;
	t->minDeg=minD;
	t->maxDeg=maxD;
	t->minMicro=minM;
	t->maxMicro=maxM;
}
 
 
/*  --------------------------------------------------------------------
    atachServo
    --------------------------------------------------------------------
    @descr:     Inicia un servomotor en un pin designado
    @param:     *t= variable usada por el usuario para manejar el servo.
    @param:  	pinS= pin equivalente para la señal del servo 
    @return:    none
    ------------------------------------------------------------------*/
void atachServo(SERVO *t,u8 pinS){
	t->pinSig=pinS;
 	t->setRange=&ponRango;
	pinmode(pinS,OUTPUT);
	t->minDeg=0;
	t->maxDeg=180;
	t->minMicro=1000;//Los rangos de vlaores se incian con los valore propuestos
	t->maxMicro=2500;//se pueden cambiar si el sero trabaja con diferentes valores
	
	t->writeD=&escribe;		
	//t->pulsos=0;
	
}

