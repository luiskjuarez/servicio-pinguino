#ifndef SERVOM_H
	#define SERVOM_H
	#include <typedef.h>
        #define MIN_DEG		0		//valores propuestos para grados y tiempos de pulso 
	#define MAX_DEG		180		//se usan en microsegundos para tener mas precicion
	#define MIN_MICRO	1000		//1ms----1.5ms------2.5ms
	#define MAX_MICRO	2500		//  |     |            |  
						//  0°----90°---------180°


 
	typedef struct srvo{  
		u8 pinSig; 			
		void (*writeD)(void *self,int grados);
		void (*setRange)(void *self,int minD,int minD,int minM,int maxM);
		int minDeg;
		int maxDeg;
		int minMicro;
		int maxMicro; 
		//int pulsos;
		
	}SERVO;  
	
	long mapea(int x,int in_min,int in_max,int out_min,int out_max);
	void escribe(void *t,int grados);
	void atachServo(SERVO *t, u8 pinS);
	void ponRango(void *self,int minD,int maxD,int minM,int maxM);
#endif
