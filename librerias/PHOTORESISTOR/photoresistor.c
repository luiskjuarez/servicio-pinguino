 /*	--------------------------------------------------------------------
    FILE:       photoresistor.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes shock sensor
    Syntaxys:
		-Declare 'PRESISTOR' type for a new sensor
			PRESISTOR t1;
		-Use global function 'atachResistor(PRESISTOR t,u8 pinS)'
			t1.atachResistor(&t1, pin for signal);
		-To read actual signal on sensor use 'readResistor'
			t1.readResistor(&t1);

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    16-07-17 : Created
    --------------------------------------------------------------------
 
    ------------------------------------------------------------------*/
 
#include <photoresistor.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalw.c>		//for pin mode y digitalwrite
#include <analog.c>	
 
 
/*  --------------------------------------------------------------------
    lee
    --------------------------------------------------------------------
    @descr:     lee el sensor		
    @param:     *self= variable que representa el sensor
    @return:    alto si esta detectando movimiento, bajo si no es asi.
    ------------------------------------------------------------------*/
u8 lee(void *self){
	PRESISTOR *t=self;
	return (analogread(t->pinSig));
    } 


/*  --------------------------------------------------------------------
    atachLaser
    --------------------------------------------------------------------
    @descr:     Inicia una fotoresistencia con un pin designado 
    @param:     *h= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachResistor(PRESISTOR *h,u8 pinS){
	h->pinSig=pinS; 
	h->readResistor=&lee;  
 
}

