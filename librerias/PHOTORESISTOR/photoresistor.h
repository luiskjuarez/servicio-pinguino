#ifndef PHOTO_H
	#define PHOTO_H
	#include <typedef.h>

 
	typedef struct ph{  
		u8 pinSig; 		//Pin para señal
		u8 (*readResistor)(void *self);
	}PRESISTOR;  

	u8 lee(void *self);
	void atachResistor(PRESISTOR *h, u8 pinS);

#endif
