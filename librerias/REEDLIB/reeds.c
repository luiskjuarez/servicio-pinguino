 /*	--------------------------------------------------------------------
    FILE:       reeds.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes magnetic field sensor, this can return the status
		of the sensor.
		
    Syntaxys:
		-Declare 'REED' type for a new magentic sensor
			REED r1;
		-Use global function 'atachReedSensor(REED r,u8 pinS)' fro inicialize Tracker sensor
			atachReedSensor(&r1, pin for signal);
		-To get status of the sensor use the function 'isNearagneticField'
			a1.isNearagneticField(&r1);	

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
   

   CHANGELOG
    --------------------------------------------------------------------
    12-07-17 : Created
    --------------------------------------------------------------------
     ------------------------------------------------------------------*/
 
#include <reeds.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalr.c>		//For digitalread instruction
#include <digitalp.c>


 
/*  --------------------------------------------------------------------
    campo
    --------------------------------------------------------------------
    @descr:     verifica si algun campo magnetico esta cerca del sensor
    @param:     *self= variable usada por el usuario para manejar el sensor IR 
    @return:    1 si hay campo, 0 si no.
    ------------------------------------------------------------------*/
int campo(void *self){
	REED *r=self;
	int senal;
	if(r->pinSig>=13 && r->pinSig<=19)
		senal=analogread(r->pinSig);
	else
		senal=digitalread(r->pinSig); 
	if(senal==HIGH || senal>=230)
		return(0);
	else
		return(1); 
} 
 
/*  --------------------------------------------------------------------
    atachReedSensor
    --------------------------------------------------------------------
    @descr:     Inicia un sensor IR nuevo en una variable usada por el usuario
    @param:     *a= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachReedSensor(REED *r,u8 pinS){
	pinmode(pinS,INPUT);
	r->pinSig=pinS;
	r->isNearagneticField=&campo;
}

