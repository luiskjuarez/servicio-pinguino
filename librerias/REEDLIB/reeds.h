#ifndef REEDS_H
	#define REEDS_H
	#include <typedef.h>
	
	
	//Estructura que contendra todos los atributos y funciones del sensor//
	typedef struct reed{  
		u8 pinSig; 			//pin para la señal del sensor
		int (*isNearagneticField)(void *self);
	}REED;  
	
	
	void atachReedSensor(REED *r, u8 pinS);
	int campo(void *self);	 
#endif
