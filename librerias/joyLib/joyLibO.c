 /*	--------------------------------------------------------------------
    FILE:       joyLibO.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes_SJois (joystick) can return actual position
		of the joystick.
    Syntaxys:
		-Declare 'joys' type for a new joystick
			joys j1;
		-Use global function 'atachJoy(joys j,u8 x,u8 y,u8 sw)' fro inicialize joystick
			atachJoy(&j1, pin for X, pin for Y, pin for Switch (-1 if not used))
		-To get x or y position use the functions getX(joys jo) and getY(joys jo) from the joys structre
			int x = j1.getX(&j1);   <-- the functions always need the same variable used
			        ^________^
		-To get status of button use the function 'isPressedButton(joys jo)'
			int statusSW1 = j1.isPressedButton(&j1);<-- the functions always need the same variable used
					^___________________^	
		
    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
  
  --------------------------------------------------------------------
    CHANGELOG
    04-07-17 : Created
    09-07-17 : Changued to structure for more joysticks
    --------------------------------------------------------------------
	This library use the analog library from pinguino
    ------------------------------------------------------------------*/
 

#include <joyLibO.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <analog.c>		//For analogread instruction
#include <digitalr.c> 


/*  --------------------------------------------------------------------
    atachJoy
    --------------------------------------------------------------------
    @descr:     Inicia las funciones del joystick para la variable usada por el usuario
    @param:     *j= variable usada por el usuario para manejar el joystick
    @param:  	x= pin equivalente para posicion en x
    @param:  	y= pin equivalente para posicion en y
    @param:	sw= pin equivalente para el switch del joystick
    @return:    none
    ------------------------------------------------------------------*/
void atachJoy(joys *j,u8 x, u8 y, u8 sw){
	j->isPressedButton=&isPressed;
	j->getX=&getrX;
	j->getY=&getrY;
	inicializeAllPin(j,x,y,sw);

}



/*  --------------------------------------------------------------------
    inicializeAllPin
    --------------------------------------------------------------------
    @descr:     inicializa los pines que utilizara el joystick
		pin x, piny, pin switch
    @param:     *jo= variable usada por el usuario para manejar el joystick
    @param:     x= pin equivalente para posicion en x
    @param:  	y= pin equivalente para posicion en y
    @param:	sw= pin equivalente para el switch del joystick
    @return:    none
    ------------------------------------------------------------------*/
void inicializeAllPin(void *jo,u8 x,u8 y, u8 sw){
	joys *j=jo;	
	j->prX=x;
	j->prY=y;
	j->joyButton=sw;
	iniciaCoordenadas(j);
	 
}


/*  --------------------------------------------------------------------
    iniciaCoordenadas
    --------------------------------------------------------------------
    @descr:     Inicia una constante con los valores iniciales del joystick
    @param:     *jo= variable usada por el usuario para manejar el joystick
    @return:    none
    ------------------------------------------------------------------*/
void iniciaCoordenadas(void *jo){
	 joys *j=jo;
		//pinmode(j->prX,INPUT);
		//pinmode(j->prY,INPUT); //Pone los pones correspondientes en modo entrada
		j->rXCons=analogread(j->prX);//lee los valores iniciales de x
		j->rYCons=analogread(j->prY);//lee los valores iniciales de y
		j->divisorX=j->rXCons/5;//inicia un divisor para mapear en 5 las posiciones del joystic
		j->divisorY=j->rYCons/5;
	 
}
 



/*  --------------------------------------------------------------------
    ispressed
    --------------------------------------------------------------------
    @descr:     indica si se presiona o no el switch del joystic
    @param:     *jo= variable usada por el usuario para manejar el joystick
    @return:    1 si se presiono, 0 si no se presiono
    ------------------------------------------------------------------*/
int isPressed(void *jo){
	joys *j=jo;
	if(  digitalread(j->joyButton)==HIGH)
		return 1;
	else
		return 0;
}


/*  --------------------------------------------------------------------
    getrX
    --------------------------------------------------------------------
    @descr:     obtiene la posicion del joystick en X actual mapeada con 5 niveles
    @param:     *jo= variable usada por el usuario para manejar el joystick
    @return:    res = posicion actual
    ------------------------------------------------------------------*/


int getrX(void *jo){
	 joys *j=jo;
		int res;
		int lectura=analogread(j->prX);
		lectura=lectura - j->rXCons;
		res=lectura/j->divisorX;
		return (res);	
	 
	 
}

/*  --------------------------------------------------------------------
    getrY
    --------------------------------------------------------------------
    @descr:     obtiene la posicion del joystick en y actual mapeada con 5 niveles
    @param:     *jo= variable usada por el usuario para manejar el joystick
    @return:    res = posicion actual
    ------------------------------------------------------------------*/

int getrY(void *jo){
	 joys *j=jo;
		int  res;
		int lectura=analogread(j->prY);
		lectura=lectura - j->rYCons;
		res=lectura/j->divisorY;
		return (res); 
}
