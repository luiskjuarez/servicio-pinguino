#ifndef JOYLIBO_H
	#define JOYLIBO_H
	#include <typedef.h>
	#include <analog.c>

	//Estructura que ocntendra todos los atributos y funciones del joystick//
	typedef struct joy{ 
		u8 joyButton; 	//Pin del boton
		u8 prX;		//Pin de x
		u8 prY;		//Pin de y
		int rXCons;	//Posicion con que inicio x
		int rYCons;	//posicion con que inicio y
		int divisorX;	//divisor para sacar escala de posicion en x
		int divisorY;	//Divisor para sacar escala de posiion en y
		void (*inicializeAllPins) (void *self,u8 x, u8 y, u8 sw); 
		int (*isPressedButton)(void *self);
		int (*getX)(void *self);
		int (*getY)(void *self);
	}joys;  

	void inicializeAllPin(void *j,u8 x,u8 y, u8 sw); 
	void iniciaCoordenadas(void *jo);
	int isPressed(void *jo );
	int getrX(void *jo ); 
	int getrY( void *jo); 
	void atachJoy(joys *j,u8 x, u8 y, u8 sw);
#endif



 
