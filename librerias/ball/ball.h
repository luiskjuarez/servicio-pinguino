#ifndef BALL_H
	#define BALL_H
	#include <typedef.h>

 
	typedef struct ball{  
		u8 pinSig; 		//Pin para señal
		u8 (*readBall)(void *self);
		u16 tiempo;
		u8 status;
		int shake;
		void (*updateBall)(void *self);
		int (*isShaking)(void *self);
	}BALL;  

	u8 lee(void *self);
	void atachBall(BALL *h, u8 pinS);
	void updateB(void *self);
	int shaking(void *self);
#endif
