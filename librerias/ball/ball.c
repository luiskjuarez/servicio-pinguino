 /*	--------------------------------------------------------------------
    FILE:       ball.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes ball sensor
    Syntaxys:
		-Declare 'BALL' type for a new sensor
			BALL t1;
		-Use global function 'atachBall(BALL t,u8 pinS)' fro inicialize a laser
			t1.atachBall(&t1, pin for signal);
		-To read actual signal on sensor use 'readBall'
			t1.readBall(&t1);
		-To know if the sensor is shaking use 'isShakig'
			t1.isShaking(&t1);
		-To update sensor values use 'updateBall'
			t1.updateBall(&t1);

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    01-08-17 : Created
    --------------------------------------------------------------------
 
    ------------------------------------------------------------------*/
 
#include <ball.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalp.c>		//for pin mode y digitalwrite
#include <digitalr.c>
#include <millis.c>		//get time
	


 
 
/*  --------------------------------------------------------------------
    shaking
    --------------------------------------------------------------------
    @descr:     regresa si esta siendo agitado o no	
    @param:     *self= variable que representa el sensor
    @return:    si esta agitado regresa 1 si no un 0
    ------------------------------------------------------------------*/
int shaking(void *self){
	BALL *t=self;
	return (t->shake);
    } 


/*  --------------------------------------------------------------------
    updateB
    --------------------------------------------------------------------
    @descr:     actualiza los valores del sensor 
    @param:     *self= variable que representa el sensor
    @return:    none
    ------------------------------------------------------------------*/
 void updateB(void *self){
	BALL *t=self;
	u8 auxState=t->status;
	if(millis()-(t->tiempo) >30){
		t->shake=0;			//Si dura mas de 30ms estable no esta agitado
		t->tiempo=millis();	
	}
	
	t->status=digitalread(t->pinSig);
	if((auxState==LOW && t->status==HIGH) ||(auxState==HIGH && t->status==LOW) ){
		if(millis()-(t->tiempo) < 15){   			//si ocurre un cambio en un lapso de 15ms esta siendo agitado
			t->shake=1;
			t->tiempo=millis();
		}
	}
	 
}
 
/*  --------------------------------------------------------------------
    lee
    --------------------------------------------------------------------
    @descr:     lee el sensor		
    @param:     *self= variable que representa el sensor
    @return:    alto si esta detectando movimiento, bajo si no es asi.
    ------------------------------------------------------------------*/
u8 lee(void *self){
	BALL *t=self;
	return (digitalread(t->pinSig));
    } 


/*  --------------------------------------------------------------------
    atachLaser
    --------------------------------------------------------------------
    @descr:     Inicia un sensor ball con un pin designado 
    @param:     *h= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachBall(BALL *h,u8 pinS){
	pinmode(pinS,INPUT);
	h->pinSig=pinS; 
	h->tiempo=0;
	h->readBall=&lee;  
	h->status=LOW;
	h->shake=0;
	h->isShaking=&shaking;
	h->updateBall=&updateB;
 
}

