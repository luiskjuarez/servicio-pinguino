 /*	--------------------------------------------------------------------
    FILE:       tracker.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes tracker line sensor, this can return the status
		of the sensor, on track or not.
	
    Syntaxys:
		-Declare 'TRACKER' type for a new tracker line sensor
			TRACKER t1;
		-Use global function 'atachTracker(TRACKER t,u8 pinS)' fro inicialize Tracker sensor
			atachTracker(&t1, pin for signal);
		-To get status of the sensor use the function 'onTrack'
			t1.onTrack(&e1);	

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    10-07-17 : Created
    --------------------------------------------------------------------
	This library use the analog library from pinguino
    ------------------------------------------------------------------*/
 
#include <tracker.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <analog.c>		//For analogread instruction
 


 
/*  --------------------------------------------------------------------
    enPista
    --------------------------------------------------------------------
    @descr:     verifica si el sensor esta en la pista (linea negra) 
    @param:     *self= variable usada por el usuario para manejar el sensor de linea 
    @return:    1 si esta en la linea, 0 si no esta en la linea
    ------------------------------------------------------------------*/
int enPista(void *self){
	TRACKER *t=self;
	int valNu=analogread(t->pinSig);
	
	if(valNu<=t->valIni+300 && valNu >=t->valIni-300)
		return(1);
	else
		return (0);

/*
	return(analogread(t->pinSig));
	if(analogread(t->pinSig)>=0&&analogread(t->pinSig)<=80)
		return 1;
	else
		return 0;
 */
} 
 
/*  --------------------------------------------------------------------
    atachTracker
    --------------------------------------------------------------------
    @descr:     Inicia un sensor de linea nuevo en una variable usada por el usuario
    @param:     *t= variable usada por el usuario para manejar el sensor de linea
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachTracker(TRACKER *t,u8 pinS){
	t->pinSig=pinS;
	t->onTrack=&enPista;
	t->valIni=analogread(t->pinSig);
}

