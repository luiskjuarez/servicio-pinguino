#ifndef TRACKER_H
	#define TRACKER_H
	#include <typedef.h>
	#include <analog.c>

	
	//Estructura que contendra todos los atributos y funciones del sensor de linea//
	typedef struct track{  
		u8 pinSig; 			//pin para la señal del sensor
		int (*onTrack)(void *self);
		int valIni;
	}TRACKER;  

	int enPista(void *self);	 
	void atachTracker(TRACKER *t, u8 pinS);

#endif
