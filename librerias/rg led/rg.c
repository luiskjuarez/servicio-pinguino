 /*	--------------------------------------------------------------------
    FILE:       rg.c
    PROJECT:    Pinguino
    PURPOSE:    Control for a RG lef (two color led)
    Syntaxys:
		-Declare 'RG' type for a new RG led
			RG l1;
		-Use global function 'atachLedRG(RG l,u8 pinR,u8 pinG)'  
			atachLedRG(&l1, pin r,pin g);
		-To write color in a led use 'writeVal' function	
			l1.writeVal(&l1,233,21);

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    15-07-17 : Created
    --------------------------------------------------------------------
	This library use the analog library from pinguino
    ------------------------------------------------------------------*/
 
#include <rg.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <analog.c>		//For digitalread instruction
#include <digitalp.c>                   // pinmode
	

	
		
	/*  --------------------------------------------------------------------
    escribe
    --------------------------------------------------------------------
    @descr:     escribe un valor en los pines que se estan usando para el led
    @param:     *self= variable usada por el usuario para manejar el sensor.
    @param:  	r= valor a escribir en el pin r
	@param:		g= valor para escribir en el pin g
    @return:    none
    ------------------------------------------------------------------*/
	void escribe(void *self,int r,int g){
		RG  *l=self;
		if(r<0)
			r=0;
		if(r>255)
			r=255;
		if(g<0)
			g=0;
		if(g>255)
			g=255;
		 
		analogwrite(l->R,r);
		analogwrite(l->G,g);
	 	
	}
	
	
	/*  --------------------------------------------------------------------
    atachLedRG
    --------------------------------------------------------------------
    @descr:     Inicia un sensor RG cin los pines indicados
    @param:     *l = variable usada para manejar le led
    @param:  	pinR = pin que se va usar para r
	@param: 	pinG = piin que se va a usar para g
    @return:    none
    ------------------------------------------------------------------*/
	void atachLedRG(RG  *l, u8 pinR,u8 pinG ){
		
		pinmode(pinR,OUTPUT);
		pinmode(pinG,OUTPUT);
		l->R=pinR;
		l->G=pinG;
		l->writeVal=&escribe;
	}
 
 
 
