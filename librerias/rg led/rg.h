#ifndef RG_H
	#define RG_H
	#include <typedef.h>

	//Estructura que contendra todos los atributos y funciones del Led//
	typedef struct rg{  
		u8 R;  //pin corresponfiente a r
		u8 G;	//pin correspondiente a g
	 
		void (*writeVal)(void *self,int r,int g );
	}RG ;  

	void escribe(void *self,int r,int g);
	void atachLedRG(RG  *l, u8 pinR,u8 pinG);

#endif
