 /*	--------------------------------------------------------------------
    FILE:       tapmodule.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes tap module
    Syntaxys:
		-Declare 'KNOK' type for a new sensor
			KNOK t1;
		-Use global function 'atachKnok(KNOK t,u8 pinS)' fro inicialize a knok sensor
			t1.atachKnok(&t1, pin for signal);
		-To read actual signal on sensor use 'readKnok'
			t1.readKnok(&t1);
		-To get actual knoks per second 'knoksPS'
			t1.knoksPS(&t1);

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    02-08-17 : Created
    --------------------------------------------------------------------
 
    ------------------------------------------------------------------*/
 
#include <tapmodule.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalp.c>		//for pin mode 
#include <digitalr.c>
#include <millis.c>		//get time



/*  --------------------------------------------------------------------
    actualizaKnok
    --------------------------------------------------------------------
    @descr:     actualiza el estado de todas las variables del sensor
    @param:     *self= variable usada por el usuario para manejar el sensor.
    @return:    none
    ------------------------------------------------------------------*/
void actualizaKnok(void *self){
	KNOK *t=self;
 
 
	t->status=digitalread(t->pinSig); //actualiza el estado actual del sensor
	if(millis()-(t->currentTime) > 1000){   
		t->nK=t->con ;
		t->con =0;
		t->currentTime=millis();
	}

	if(millis()-(t->time2) >.5 && t->banK1==1){  
		t->banK=0;
		t->banK1=0; 
 		t->con++;
	}
	if(t->status==HIGH && t->banK==0){ 
			t->banK=1; 
	}
	else
		if(t->status==LOW && t->banK==1){
			t->time2=millis();			
			t->banK1=1; 
		}  
}
 


/*  --------------------------------------------------------------------
    knoksPorSegundo
    --------------------------------------------------------------------
    @descr:     calcula el numero de golpes registrados los ultimos 30 segundos para despues pasarlo a minuto
    @param:     *self= variable que representa el sensor
    @return:    numero de golpes por minuto.
    ------------------------------------------------------------------*/
u8 knoksPorSegundo(void *self){
	KNOK *t=self;
	return (t->nK);
} 				




/*  --------------------------------------------------------------------
    leeKnok
    --------------------------------------------------------------------
    @descr:     lee el estdo actual del sensor		
    @param:     *self= variable que representa el sensor
    @return:    alto si esta detectando un golpe bajo si no.
    ------------------------------------------------------------------*/
u8 leeKnok(void *self){
	KNOK *t=self;
	return (t->status);
    } 


/*  --------------------------------------------------------------------
    atachKnok
    --------------------------------------------------------------------
    @descr:     Inicia un sensor knok con un pin designado 
    @param:     *h= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachKnok(KNOK *h,u8 pinS){
	int i;
	pinmode(pinS,INPUT);
	h->pinSig=pinS; 
	h->readKnok=&leeKnok;  
	h->knoksPS=&knoksPorSegundo;
	h->updateKnok=&actualizaKnok;
	h->status=0;
	h->currentTime=0;
	h->con=0;
	h->banK=0;
	h->nK=0;
	h->time2=0;
	h->banK1=0;
}

