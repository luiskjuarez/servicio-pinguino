#ifndef TAP_H
	#define TAP_H
	#include <typedef.h>

 
	typedef struct tap{  
		u8 pinSig; 		//Pin para señal
		u8 status;		//estado actual del sensor
		u16 currentTime;	//tiempo transcurrido (se actualiza cada 300 ms)
		u8 (*readKnok)(void *self);
		u8 (*knoksPS)(void *self); 
		void (*updateKnok)(void *self);
		int con;		//Contador para knoks auxiliar
		int banK;
		int nK;			//numero Knoks
		u16 time2;		//timepo papra esperar el sensor
		int banK1;
	}KNOK;  

	u8 leeKnok(void *self);
	void atachKnok(KNOK *h, u8 pinS);
	u8 knoksPorSegundo(void *self);
#endif
