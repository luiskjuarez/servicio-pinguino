#ifndef AVOIDIR_H
	#define AVOIDIR_H
	#include <typedef.h>
	
	
	//Estructura que contendra todos los atributos y funciones del sensor//
	typedef struct avoid{  
		u8 pinSig; 			//pin para la señal del sensor
		int (*isObstructing)(void *self);
	}AVOID;  
	
	int obstruyendo(void *self);	 
	void atachIrSensor(AVOID *a, u8 pinS);

#endif
