 /*	--------------------------------------------------------------------
    FILE:       avoidir.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes IR avoidance sensor, this can return the status
		of the sensor, obstructing or not.
	
    Syntaxys:
		-Declare 'AVOID' type for a new IR sensor
			AVOID a1;
		-Use global function 'atachIrSensor(AVOID a,u8 pinS)' for inicialize new IR sensor
			atachIrSensor(&a1, pin for signal);
		-To get status of the sensor use the function 'isObstructing'
			a1.isObstructing(&e1);	

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    12-07-17 : Created
    --------------------------------------------------------------------
	This library use the digital library from pinguino
    ------------------------------------------------------------------*/
 
#include <avoidir.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalr.c>		//For digitalread instruction
 


 
/*  --------------------------------------------------------------------
    obstruyendo
    --------------------------------------------------------------------
    @descr:     verifica si algun objeto obstruye el sensor
    @param:     *self= variable usada por el usuario para manejar el sensor IR 
    @return:    1 si obstruido, 0 si no.
    ------------------------------------------------------------------*/
int obstruyendo(void *self){
	AVOID *a=self;
	int senal=digitalread(a->pinSig); 
	if(senal==HIGH || senal>=230)
		return(0);
	else
		return(1); } 
 
/*  --------------------------------------------------------------------
    atachIrSensor
    --------------------------------------------------------------------
    @descr:     Inicia un sensor IR nuevo en una variable usada por el usuario
    @param:     *a= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachIrSensor(AVOID *a,u8 pinS){
	a->pinSig=pinS;
	a->isObstructing=&obstruyendo;
}

