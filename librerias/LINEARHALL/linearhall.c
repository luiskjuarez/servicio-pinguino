 /*	--------------------------------------------------------------------
    FILE:       linearhall.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes linear hall sensor
    Syntaxys:
		-Declare 'LHALL' type for a new sensor
			LHALL a1;
		-Use global function 'atachLinearHall(LHALL a,u8 pinS)' for inicialize magnetic sensor
			atachLinearHall(&a1, pin for signal);
		-To get analogic signal use 'analogHall(LHALL a)'function
			a1.analogHall(&a1);
		-To get digital signal use 'digitalHall(LHALL a)'function
			a1.digitalHall(&a1);
			

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    16-07-17 : Created
    --------------------------------------------------------------------
	This library use the digital library from pinguino
    ------------------------------------------------------------------*/
 
#include <linearhall.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalr.c>		//For digitalread instruction
#include <analog.c>
	

	/*  --------------------------------------------------------------------
    analogo
    --------------------------------------------------------------------
    @descr:     obtiene la señal analogica del sensor utilizado
    @param:     *self= variable que representa el sensor utilizado
    @return:    señal analogica comprendida entre 0 y 255
    ------------------------------------------------------------------*/
int analogo(void *self){
	LHALL *l=self;
	return  (analogread(l->pinSig));   
 }
 
 
 
 /*  --------------------------------------------------------------------
    digital
    --------------------------------------------------------------------
    @descr:     obtiene la señal digital del sensor
    @param:     *self= variable que representa el sensor utilizado
    @return:    1 o 0 (HIGH o LOW)
    ------------------------------------------------------------------*/ 
u8 digital(void *self){
	LHALL *l=self;
	return  (digitalread(l->pinSig));  
 }
 
 
/*  --------------------------------------------------------------------
    atachLinearHall
    --------------------------------------------------------------------
    @descr:     Inicia un sensor LHALL nuevo en una variable usada por el usuario
    @param:     *l= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachLinearHall(LHALL *l,u8 pinS){
	l->pinSig=pinS;
	l->analogHall=&analogo;
	l->digitalHall=&digital;
}

