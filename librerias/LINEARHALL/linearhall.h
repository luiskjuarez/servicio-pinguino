#ifndef LINEARH_H
	#define LINEARH_H
	#include <typedef.h>

	//Estructura que contendra todos los atributos y funciones del sensor de linea//
	typedef struct lnhall{  
		u8 pinSig; 			//pin para la señal del sensor
		int (*analogHall)(void *self);
		u8 (*digitalHall)(void *self); 
	}LHALL;  

	int analogo(void *self);	 
	u8 digital(void *self);
	void atachLinearHall(LHALL *l, u8 pinS);

#endif
