 /*	--------------------------------------------------------------------
    FILE:       rgb.c
    PROJECT:    Pinguino
    PURPOSE:    Manipulate an RGB led 
    Syntaxys:
		-Declare 'RGB' type for a new rgb led
			RGB l1;
		-Use global function 'atachLed(RGB a, u8 pinR,u8 pinG,u8 pinB)' for inicialize a new led
			atachLed(&l1, pinR, pinG, pinB);
		-TO write value on a led use the function 'writeVal(RGB l, int valr,int valg,int valb)'
			l1.writeVal(&l1,value r, value g, value b);

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    15-07-17 : Created
    --------------------------------------------------------------------
	This library use the digital library from pinguino
    ------------------------------------------------------------------*/
 
#include <rgb.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <analog.c>		//For digitalread instruction
#include <digitalp.c>                   // pinmode
	

	
		
/*  --------------------------------------------------------------------
    escribe
    --------------------------------------------------------------------
    @descr:     escribe un valor en los pines que se estan usando para el led
    @param:     *self= variable usada por el usuario para manejar el sensor.
    @param:  	r= valor a escribir en el pin r
	@param:		g= valor para escribir en el pin g
	@param:		b= valor para escribir en el pin b
    @return:    none
    ------------------------------------------------------------------*/
	void escribe(void *self,int r,int g,int b){
		RGB *l=self;
		if(r<0)
			r=0;
		if(r>255)
			r=255;
		if(g<0)
			g=0;
		if(g>255)
			g=255;
		if(b<0)
			b=0;
		if(b>255)
			b=255; 
		analogwrite(l->R,r);
		analogwrite(l->G,g);
		analogwrite(l->B,b); 		
	}
	
	
		/*  --------------------------------------------------------------------
    atachLed
    --------------------------------------------------------------------
    @descr:     Inicia un sensor RG cin los pines indicados
    @param:     *l = variable usada para manejar le led
    @param:  	pinR = pin que se va usar para r
	@param: 	pinG = piin que se va a usar para g
	@param: 	pinB = piin que se va a usar para b 
    @return:    none
    ------------------------------------------------------------------*/
	void atachLed(RGB *l, u8 pinR,u8 pinG,u8 pinB){
		
		pinmode(pinR,OUTPUT);
		pinmode(pinG,OUTPUT);
		pinmode(pinB,OUTPUT);
		l->R=pinR;
		l->G=pinG;
		l->B=pinB;
		l->writeVal=&escribe;
	}
 
 
 
