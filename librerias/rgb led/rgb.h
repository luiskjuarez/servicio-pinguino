#ifndef RGB_H
	#define RGB_H
	#include <typedef.h>

	//Estructura que contendra todos los atributos y funciones del sensor de linea//
	typedef struct digtemp{  
		u8 B;  //pin correspondiente a b
	 	u8 R;  //pin corresponfiente a r
		u8 G;  //pin correspondiente a g 
		void (*writeVal)(void *self,int r,int g,int b); 
	}RGB;  

	void escribe(void *self,int r,int g,int b); 
	void atachLed(RGB *l,u8 pinR,u8 pinG,u8 pinB);

#endif
