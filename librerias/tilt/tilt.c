 /*	--------------------------------------------------------------------
    FILE:       tilt.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes tilt sensor
    Syntaxys:
		-Declare 'TILT' type for a new sensor
			TILT t1;
		-Use global function 'atachTilt(TILT t,u8 pinS)' fro inicialize a laser
			t1.atachTilt(&t1, pin for signal, pin for led);
		-To read actual signal on sensor use 'readTilt'
			t1.readTilt(&t1);
		-To know if the sensor is shaking use 'isShakig'
			t1.isShaking(&t1);
		-To update sensor values use 'updateCup'
			t1.updateTilt(&t1);

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    29-07-17 : Created
    --------------------------------------------------------------------
 
    ------------------------------------------------------------------*/
 
#include <tilt.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalp.c>		//for pin mode y digitalwrite
#include <digitalr.c>
#include <millis.c>		//get time
	


 
 
/*  --------------------------------------------------------------------
    shaking
    --------------------------------------------------------------------
    @descr:     regresa si esta siendo agitado o no	
    @param:     *self= variable que representa el sensor
    @return:    si esta agitado regresa 1 si no un 0
    ------------------------------------------------------------------*/
int shaking(void *self){
	TILT *t=self;
	return (t->shake);
    } 


/*  --------------------------------------------------------------------
    updateT
    --------------------------------------------------------------------
    @descr:     actualiza los valores del sensor 
    @param:     *self= variable que representa el sensor
    @return:    none
    ------------------------------------------------------------------*/
 void updateT(void *self){
	TILT *t=self;
	u8 auxState=t->status;
	if(millis()-(t->tiempo) >30){
		t->shake=0;			//Si dura mas de 30ms estable no esta agitado
		t->tiempo=millis();	
	}
	
	t->status=digitalread(t->pinSig);
	if((auxState==LOW && t->status==HIGH) ||(auxState==HIGH && t->status==LOW) ){
		if(millis()-(t->tiempo) < 15){   			//si ocurre un cambio en un lapso de 15ms esta siendo agitado
			t->shake=1;
			t->tiempo=millis();
		}
	}
	 
}
 
/*  --------------------------------------------------------------------
    lee
    --------------------------------------------------------------------
    @descr:     lee el sensor		
    @param:     *self= variable que representa el sensor
    @return:    alto si esta detectando movimiento, bajo si no es asi.
    ------------------------------------------------------------------*/
u8 leeT(void *self){
	TILT *t=self;
	return (digitalread(t->pinSig));
    } 


 
/*  --------------------------------------------------------------------
    atachTilt
    --------------------------------------------------------------------
    @descr:     Inicia un sensor ball con un pin designado 
    @param:     *h= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachTilt(TILT *h,u8 pinS){
	pinmode(pinS,INPUT);
	h->pinSig=pinS; 
	h->tiempo=0;
	h->readTilt=&leeT;  
	h->status=LOW;
	h->shake=0;
	h->isShaking=&shaking;
	h->updateT=&ultpdateTilt;
 
}

