#ifndef TILT_H
	#define TILT_H
	#include <typedef.h>

 
	typedef struct tilt{  
		u8 pinSig; 		//Pin para señal
		u8 (*readTilt)(void *self);
		u16 tiempo;
		u8 status;
		int shake;
		u8 pinL;
		void (*updateTilt)(void *self);
		int (*isShaking)(void *self);
 
	}TILT;  

	u8 leeT(void *self);
	void atachTilt(TILT *h, u8 pinS);
	void updateT(void *self);
	int shaking(void *self);
 
#endif
