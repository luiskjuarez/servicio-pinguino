/*	--------------------------------------------------------------------
    FILE:       DHT11.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes magnetic sensor
    Syntaxys:
		-Declare 'DHT' type for a new sensor
			DHT t1;
		-Use global function 'atachDHT(DHT t,u8 pinS)' fro inicialize a new DHT
			t1.atachDHT(&t1, pin for signal);
		-To read temperature use 'readTemp'
			t1.readTemp(&t1);
		-To read humidity use 'readHumidity'
			t1.readHumidity(&t1);
		-To update sensor vlaues use 'updateDHT'
			t1.updateDHT(&t1);
			!!This function should be in the loop function!!

    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    07-08-17 : Created
    --------------------------------------------------------------------
 
    ------------------------------------------------------------------*/
 
#include <DHT11.h>		//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalw.c>		//for pin mode y digitalwrite
#include <digitalr.c>
#include <digitalp.c>	
#include <delayus.c> 
#include <delayms.c>
 


/*  --------------------------------------------------------------------
    readData
    --------------------------------------------------------------------
    @descr:     lee los 40 bits enviados por el sensor
    @param:     pin= pin digital del sensor
    @param:  	*self= pin equivalente para la señal del sensor 
    @param:	iV= numero de byte que se esta leyendo (5 bytes en total)
    @return:    byte leido
    ------------------------------------------------------------------*/	
byte readData(u8 pin,void *self,int iV){
	byte data;
	DHT *t=self;
	int v[8];
	int i;
 
	  	for (i = 0; i < 8; i ++) {
			
			while(digitalread(pin)==LOW); ///TALVEZ VAYA TAlVEZ NO
	    		//if (digitalread (pin) == LOW) {
	      			//Delayus(50);//while (digitalread (pin) == LOW);  
					      			
				Delayus(35);  
	      			
				if (digitalread (pin) == HIGH){
					t->Vals[iV][i]=1;
	      				while (digitalread (pin) == HIGH); 
					}
				else{ 
					t->Vals[iV][i]=0; 
					} 
	     		//}
	 	 } 
	return data;
}
 



/*  --------------------------------------------------------------------
    checkParityBits
    --------------------------------------------------------------------
    @descr:     checa si el dato leido es correcto, si la suma de los valores de temperatura y humedad es igual a el byte de checksum
		es correcto
    @param:     *self= variable usada por el usuario para manejar el sensor.
    @return:    1 si es correcto 0 si no.
    ------------------------------------------------------------------*/
int checkParityBits(void *self){
	int res,valCheckSum;
	int i;
	DHT *t=self;
	res=0;
	
	for(i=0;i<4;i++)
		res+=convertByteToInt(t->Vals[i]);
	valCheckSum=convertByteToInt(t->Vals[4]);
	if(res==valCheckSum)
		return 1;

	return 0;
}


/*  --------------------------------------------------------------------
    convertByteToInt
    --------------------------------------------------------------------
    @descr:     convierte un byte en int
    @param:     arr= arreglo de enteros (1 o 0)
    @return:    valor del byte
    ------------------------------------------------------------------*/
int convertByteToInt(int arr[8]){
	int i;	
	int val=0;
	for(i=7;i>=0;i--){
		if(arr[i]==1)
			val+=(int)(potencia(2,7-i));	
	}
	return val;

}



/*  --------------------------------------------------------------------
    leeTemp
    --------------------------------------------------------------------
    @descr:     lee la ultima temperatura guardada del sensor
    @param:     *self= variable usada por el usuario para manejar el sensor.
    @return:    temperatura leida
    ------------------------------------------------------------------*/
float leeTemp(void *self){	
	DHT *t=self;
	return t->temper; 
}	 





/*  --------------------------------------------------------------------
    leeHumedad
    --------------------------------------------------------------------
    @descr:     lee la ultima humedad guardada del sensor
    @param:     *self= variable usada por el usuario para manejar el sensor.
    @return:    humedad leida  lcd.print(" >");
    ------------------------------------------------------------------*/
float leeHumedad(void *self){
	DHT *t=self;
	return t->humeda;
}




/*  --------------------------------------------------------------------
    potencia
    --------------------------------------------------------------------
    @descr:     calcula la potencia pot de un numero n
    @param:     n= numero del que se quiere calcular la potencia
    @param:  	pot= potencia que se quiere calcular
    @return:    potencia de un numero n^pot
    ------------------------------------------------------------------*/
long potencia(int n,int pot){
	long r=n;
	int i;
	for(i=0;i<(pot-1);i++)
		r*=n;
	if(pot==0)
		return 1;
	return r;
}	

	 

/*  --------------------------------------------------------------------
    updateDHT
    --------------------------------------------------------------------
    @descr:     actualiza los valores dle sensor para poder ser leidos
		los guarda en la estructura del sensor
    @param:     *self= sensor que se quiere actualizar
    @return:    none
    ------------------------------------------------------------------*/
void updateDH(void *self){
	int i=0;
	DHT *t=self;
	pinmode(t->pinSig,OUTPUT);
 
	//Señal start
	digitalwrite (t->pinSig, LOW); 
  	Delayus(18500);//  
	pinmode(t->pinSig,INPUT);
 
	Delayus(40);

while(digitalread(t->pinSig)==HIGH);

	//Espera respuesta del sensor
	
 	Delayus(54);
	//if(digitalread(t->pinSig)==LOW)
	//	return; 
	Delayus(70);
	while(digitalread(t->pinSig)==HIGH);
	//if(digitalread(t->pinSig)==HIG
  	//while(digitalread (t->pinSig)==HIGH);
  	//Delayus(80);// Wait for DHT11 response
 	//while(digitalread(t->pinSig)==LOW);
	//Delayus(80);
	
  
	 //Lee los valores que envia el sensor (40 bits, 2 bytes para temperatura, 2 para humedad y el ultimo para checksum)
  	for (i ; i < 5; i ++) 
    		t->values[i] = readData(t->pinSig,self,i); 

 
  	//Una vez que recibe todos los 40 bits, se verifican los bits de paridad
	//si son correctos los datos se guardan en las variables correspondients
	//si no no se hace nada y se quedna los valores anteirores
	if(checkParityBits(self)==1)
	{
		t->humeda=convertByteToInt(t->Vals[0]);
		t->temper=convertByteToInt(t->Vals[2]);
	}
	//----------------------------------------------------------//
	
  	pinmode (t->pinSig, INPUT);	//espera un tiempo por si aun no termina de enviar algun dato
 	Delayus(1200);
  	///digitalwrite (t->pinSig, HIGH);  //Termino.
}
 
 

/*  --------------------------------------------------------------------
    atachDHT
    --------------------------------------------------------------------
    @descr:     inicia un nuevo sensor de humedad con un pindigital
    @param:     *h= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachDHT(DHT *h, u8 pinS){
	int i; 
	pinmode(pinS,INPUT);
	h->pinSig=pinS; 
	h->updateDHT11=&updateDH;
	h->readTemp=&leeTemp;
	h->readHumidity=&leeHumedad;
	h->values[0]=0;
	h->values[1]=0;
	h->values[2]=0;
	h->values[3]=0;
	h->temper=0;
	h->humeda=0;
	for(i=0;i<8;i++)
	{
		h->Vals[0][i]=0;
		h->Vals[1][i]=0;
		h->Vals[2][i]=0;
		h->Vals[3][i]=0;
		h->Vals[4][i]=0;

	}
	Delayms(20);
	pinmode(pinS,INPUT);
	digitalwrite(pinS,HIGH);

}
	
