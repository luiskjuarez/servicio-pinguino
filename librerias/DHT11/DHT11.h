#ifndef DHT_H
	#define DHT_H
	#include <typedef.h>

	//Estructura que contendra todos los atributos y funciones del sensor//
	typedef struct dht{  
		u8 pinSig; 			//pin para la señal del sensor
		void (*updateDHT11)(void *self);
		float (*readTemp)(void *self); 
		float (*readHumidity)(void *self);
		byte values[5];
		int Vals[5][8];
		float temper;
		float humeda;
	
	}DHT;  

	float leeTemp(void *self);	 
	float leeHumedad(void *self);
	int convertByteToInt(int arr[8]);
	int checkParityBits(void *self);
	byte readData(u8 pin,void *self,int iV);
	void updateDH(void *self);
	void atachDHT(DHT *h, u8 pinS);
	long potencia(int n,int pot);

#endif



