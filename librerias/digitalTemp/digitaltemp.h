#ifndef DIGITALT_H
	#define DIGITALT_H 
	#include <typedef.h>
 
	typedef struct digtemp{  
		u8 pinSig; 			//pin para la señal del sensor
		int (*analogTemp)(void *self);
		u8 (*digitalTemp)(void *self); 
		void (*setConstants)(void *self,long a,long b, long c);
		long A ;
		long B ;  //A B C son constantes que indica el fabricante del termistor
		long C ; //se usaron las correspondientes al temristor que usa arduino (son las propuestas por el fabricante, puede variar un poco)
	
	}DIGTEMP;  
	//estructura de apoyo para calcular logaritmos//
	typedef union
	{
	    u32 i;
	    float f;
	} fasthelper; 
	
	long logTem (long x); 
	int analogTem(void *self);	 
	u8 digital(void *self);
	void ponCons(void *self,long a,long b, long c);
	void atachDigTemp(DIGTEMP *l, u8 pinS);

#endif
