 /*	--------------------------------------------------------------------
    FILE:       digitaltemp.c
    PROJECT:    Pinguino
    PURPOSE:    Basic instructions for a keyes digital temperature sensor
	
    Syntaxys:
		-Declare 'DIGTEMP' type for a new temperatur sensor
			DIGTEMP d1;
		-Use global function 'atachDigTemp(DIGTEMP d1,u8 pinS)' fro inicialize Tracker sensor
			atachDigTemp(&d1, pin for signal);
		-To get digital signal use 'digitalTemp(DIGTEMP t1)' function
			d1.digitalTemp(&d1);
		-To get analog signal use 'analogTemp(DIGTEMP t1)'function
			d1.analogTemp(&d1);
			
    PROGRAMER:  Luis Alberto Juarez Monsivas  kito_park@hotmail.com
    --------------------------------------------------------------------
    CHANGELOG
    16-07-17 : Created
    --------------------------------------------------------------------
	This library use the digital library from pinguino
    ------------------------------------------------------------------*/
 
#include <digitaltemp.h>	//Header
#include <typedef.h>            // Pinguino's types (u8, u16, ...)
#include <const.h>   	        // HIGH, LOW, OUTPUT, ... 
#include <digitalr.c>		//For digitalread instruction
#include <analog.c>
	


 
/*  --------------------------------------------------------------------
    analogo
    --------------------------------------------------------------------
    @descr:     obtiene la señal analogica del sensor utilizado
    @param:     *self= variable que representa el sensor utilizado
    @return:    señal analogica comprendida entre 0 y 255
    ------------------------------------------------------------------*/
int analogTem(void *self){
	DIGTEMP *t=self;
	int lectura;
	long temp; 
	long Aa;
	long Bb;
	lectura=analogread(t->pinSig); 
	Aa=(1024.0/lectura-1);
	Bb=10000.0*Aa;
	temp=logTem(Bb);
 
	Aa=t->C*temp;
	Aa=Aa*temp;
	Aa=Aa*temp;
	Bb=t->B+Aa;
	Aa= t->A+Bb;
	temp= Aa/Aa;

	//temp =   1 / (t->A + (t->B + (t->C * temp * temp ))* temp ); //Formula de steinhart-hart
	temp = temp - 273.15;  // Kelvin a centigrados                     
        return temp;
	   
 }
 
 
 
 /*  --------------------------------------------------------------------
    digital
    --------------------------------------------------------------------
    @descr:     obtiene la señal digital del sensor
    @param:     *self= variable que representa el sensor utilizado
    @return:    1 o 0 (HIGH o LOW)
    ------------------------------------------------------------------*/ 
u8 digital(void *self){
	DIGTEMP *t=self;  
	return  (digitalread(t->pinSig));  
 }



 /*  --------------------------------------------------------------------
    ponCons
    --------------------------------------------------------------------
    @descr:     Da vaor a las constantes A,B,C
    @param:     *self= variable que representa el sensor utilizado
    @param:	a constante A
    @param: 	b constante B
    @param: 	c constanteC
    @return:    none
    ------------------------------------------------------------------*/ 
void ponCons(void *self,long a,long b, long c){
	DIGTEMP *t=self;  
	t->A=a;
	t->B=b;
	t->C=c;
}




 /*  --------------------------------------------------------------------
    logaritmo
    --------------------------------------------------------------------
    @descr:     calcula logaritmo
    @param:   	x valor al cual se le saca logaritmo
    @return:    logaritmo de x
    ------------------------------------------------------------------*/ 
long logTem (long x)
{
  float y;
  fasthelper vx, mx;
  vx.f = x;
  mx.i = (vx.i & 0x007FFFFF) | 0x3f000000;
  y = vx.i;
  y *= 1.1920928955078125e-7f;

  y= y - 124.22551499f
           - 1.498030302f * mx.f 
           - 1.72587999f / (0.3520887068f + mx.f);
  return (0.30102999f *  y);
}

 


 /*  --------------------------------------------------------------------
    atachDigTemp
    --------------------------------------------------------------------
    @descr:     Inicia un sensor de tempratura nuevo en una variable usada por el usuario
    @param:     *t= variable usada por el usuario para manejar el sensor.
    @param:  	pinS= pin equivalente para la señal del sensor 
    @return:    none
    ------------------------------------------------------------------*/
void atachDigTemp(DIGTEMP *t,u8 pinS){
	t->pinSig=pinS;
	t->analogTemp=&analogTem;
	t->digitalTemp=&digital;
	t->setConstants=&ponCons;
	t->A = 0.001129148;
	t-> B = 0.000234125;
	t-> C = 0.0000000876741;
}

